import React from 'react';
import Layout from '../../components/layout';
import { About, Terms, Events, Videos, Jobs } from '../../components/aside';
import Metadata from '../../components/metadata';
import './index.scss';

import imgAvatar from './images/avatar.png';
import imgAvatar2x from './images/avatar@2x.png';
import imgVideo1 from './images/video-1.png';
import imgVideo12x from './images/video-1@2x.png';
import imgVideo2 from './images/video-2.png';
import imgVideo22x from './images/video-2@2x.png';

const Page = () => (
  <Layout>
    {/* main */}
    <main className="layout__main" id="article">
      <h1 className="title">Function and Regulation of Phase-Separated Biological Condensates</h1>

      {/* metadata */}
      <Metadata />

      {/* in context */}
      <section className="analysis">
        <h2 className="analysis__title">In context</h2>
        <div className="analysis__inner">
          <header className="analysis__header">
            <img
              className="analysis__image"
              src={imgAvatar}
              srcSet={`${imgAvatar} 1x, ${imgAvatar2x} 2x`}
              alt=""
            />
            <div>
              <p>
                <strong>Drew Hawthorne</strong> <br /> PhD Scientist, Dewpoint
              </p>
              <p>
                <a href="./">See all posts</a> | <a href="./">Visit Drew’s website</a>
              </p>
            </div>
          </header>
          <div className="analysis__main">
            <p>
              Lorem ipsum dolor sit amet, ut sit ridens pertinacia, affert quidam minimum ut usu.
              Vel ea justo iudicabit cotidieque, pri ex quando suscipiantur voluptatibus. Ex falli
              electram incorrupte has, cum ad inermis postulant dissentias. Ut nam cibo tibique
              intellegebat. Eos meis voluptua dissentiunt ei, has no praesent tractatos maiestatis.
              Brute dissentiet quo cu, per ea ridens suavitate.
            </p>
            <p>
              In nullam dicunt vis, facer salutandi reprimique id cum, ex partem eligendi percipit
              est. Vel no aliquip dissentias, ex mel movet zril tation. Iisque forensibus quo ut.
              Condensation ei. Ne eleifend mandamus salutatus cum.
            </p>
            <p>
              Liber sententiae ne quo, causae vidisse sit ad. An modus luptatum consequuntur vel.
              Errem timeam scripta an pro, tollit volutpat intellegat an usu, eos et debitis
              urbanitas. No vel vide wisi, cu fierent ancillae accumsan pro. An dico accusam sea, ad
              mea prima prompta mnesarchum, nobis accusata cu sed.
            </p>
            <p>
              Usu in veniam tritani inimicus. Case signiferumque eu eum. Cibo semper salutandi ne
              cum, veri oratio ea has, ut graeci recteque usu. Velit legendos duo ex, alii dolore ea
              vix. Diceret percipit vix ea, cibo causae numquam et has. Ut sit consequat honestatis.
            </p>
          </div>
        </div>
      </section>

      {/* glossary */}
      <section className="section">
        <header className="section__header">
          <h2 className="section__title">Glossary terms</h2>
          <a className="section__see-all-link" href="./">
            See all terms
          </a>
        </header>
        <section className="section__subsection term">
          <h3 className="term__title">Condensation</h3>
          <p className="term__description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore lorem ipsum dolor sit
          </p>
        </section>
        <section className="section__subsection term">
          <h3 className="term__title">Condensates and Mutation</h3>
          <p className="term__description">
            Vel no aliquip dissentias, ex mel movet zril tation. Iisque forensibus quo ut. Est
            quando inermis ei. Ne eleifend mandamus salutatus cum.
          </p>
        </section>
      </section>

      {/* videos */}
      <section className="section">
        <header className="section__header">
          <h2 className="section__title">Related videos</h2>
          <a className="section__see-all-link" href="./">
            See all videos
          </a>
        </header>
        <figure className="section__subsection video">
          <a href="./">
            <img
              className="video__thumbnail"
              src={imgVideo1}
              srcSet={`${imgVideo1} 1x, ${imgVideo12x} 2x`}
              alt=""
            />
            <figcaption className="video__caption">
              Tony Hyman on how biomolecular condensates can treat disease.
            </figcaption>
          </a>
        </figure>
        <figure className="section__subsection video">
          <a href="./">
            <img
              className="video__thumbnail"
              src={imgVideo2}
              srcSet={`${imgVideo2} 2x, ${imgVideo22x} 2x`}
              alt=""
            />
            <figcaption className="video__caption">
              Function and Regulation of Phase-Separated Biological Condensates
            </figcaption>
          </a>
        </figure>
      </section>
    </main>

    {/* aside */}
    <aside className="layout__aside">
      <About />
      <Terms />
      <Events />
      <Videos />
      <Jobs />
    </aside>
  </Layout>
);

export default Page;
