import React from 'react';
import Layout from '../../components/layout';
import { About, Terms, Events, Videos, Jobs } from '../../components/aside';

const Page = () => (
  <Layout
    title="Infectious Disease"
    category="infectious-disease"
    intro="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore lorem ipsum dolor sit">
    {/* main */}
    <main className="layout__main" id="article"></main>

    {/* aside */}
    <aside className="layout__aside">
      <About />
      <Terms />
      <Events />
      <Videos />
      <Jobs />
    </aside>
  </Layout>
);

export default Page;
