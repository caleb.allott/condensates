import React from 'react';
import Layout from '../../components/layout';
import { About, Terms, Events, Videos, Jobs } from '../../components/aside';

import './index.scss';

import imgAvatar from './images/avatar.png';
import imgAvatar2x from './images/avatar@2x.png';

const Page = () => (
  <Layout>
    {/* main */}
    <main className="layout__main" id="home">
      <header className="header">
        <p className="header__sort">Most Recent</p>
        <select className="header__filter">
          <option defaultValue disabled>
            Filter by
          </option>
          <option>Articles</option>
          <option>Events</option>
          <option>Jobs</option>
          <option>Videos</option>
          <option>Glossary</option>
        </select>
      </header>

      {/* post with analysis */}
      <section className="post article">
        <p className="article__subtitle">June 2, 2019 | Cell.com</p>
        <h2 className="article__title">
          Function and Regulation of Phase-Separated Biological Condensates
        </h2>
        <ul className="article__categories categories">
          <li>
            <a className="category category_cancer" href="./">
              Cancer
            </a>
          </li>
          <li>
            <a className="category category_infectious-disease" href="./">
              Infectious Disease
            </a>
          </li>
          <li>
            <a className="category category_transcription" href="./">
              Transcription
            </a>
          </li>
          <li>
            <a className="categories__overflow" href="./">
              + 3 more
            </a>
          </li>
        </ul>
        <section className="analysis">
          <img
            className="analysis__image"
            src={imgAvatar}
            srcSet={`${imgAvatar} 1x, ${imgAvatar2x} 2x`}
            alt=""
          />
          <div>
            <h3 className="analysis__title">Analysis | Drew Hawthorne, PhD Scientist</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor...
            </p>
          </div>
        </section>
      </section>

      {/* sponsored */}
      <section className="post job">
        <strong className="badge badge_gold">Sponsored</strong>
        <h2 className="job__title">Cell Biologist, Drug Discovery Technology</h2>
        <p className="job__subtitle">Dewpoint Therapeutics | Dresden, Germany</p>
        <a className="button" href="./">
          Apply
        </a>
      </section>

      {/* event */}
      <section className="post event">
        <p>Upcoming event</p>
        <div className="event__wrapper">
          <div className="event__calendar calendar" aria-hidden>
            <div className="calendar__month">Aug</div>
            <div className="calendar__day">30</div>
          </div>
          <div className="event__main">
            <h2 className="event__title">Phase separation galla</h2>
            <p className="event__subtitle">August 30, 2019 | Boston, MA</p>
            <p className="event__description">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor...
            </p>
          </div>
        </div>
      </section>

      {/* discussion */}
      <section className="post discussion">
        <p>Trending term | 4 people talking about...</p>
        <h2 className="discussion__title">Formation of biological condensates</h2>
        <div className="discussion__thread">
          <section className="analysis">
            <img
              className="analysis__image"
              src={imgAvatar}
              srcSet={`${imgAvatar} 1x, ${imgAvatar2x} 2x`}
              alt=""
            />
            <div>
              <h3 className="analysis__title">Analysis | Drew Hawthorne, PhD Scientist</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor...
              </p>
            </div>
          </section>
          <section className="discussion__reply">
            <h3 className="discussion__reply-title">Mika Slivoka, Cell Biologist</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</p>
          </section>
        </div>
        <a className="button" href="./">
          Join discussion
        </a>
      </section>
    </main>

    {/* aside */}
    <aside className="layout__aside">
      <About />
      <Terms />
      <Events />
      <Videos />
      <Jobs />
    </aside>
  </Layout>
);

export default Page;
