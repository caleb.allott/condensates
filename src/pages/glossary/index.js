import React from 'react';
import Layout from '../../components/layout';
import { JumpTo, PopularTerms, BrowseTerms } from '../../components/aside';
import Metadata from '../../components/metadata';
import './index.scss';

const Page = () => (
  <Layout>
    {/* main */}
    <main className="layout__main" id="glossary">
      <nav className="nav">
        <ul className="nav__links-list">
          <li>
            <a href="./" className="nav__link nav__link_active">
              Guide
            </a>
          </li>
          <li>
            <a href="./" className="nav__link">
              Terms
            </a>
          </li>
        </ul>
        <form>
          <label className="find__label">Find a term</label>
          <input className="find__input" placeholder="Enter term..." />
          <button className="find__button" aria-label="Submit" />
        </form>
      </nav>

      <h1 className="title">
        Formation of biological condensates: A glossary of terms and concepts
      </h1>

      <Metadata />

      <section className="section">
        <h2 className="section__title">Introduction</h2>
        <div className="intro">
          <p>
            Cells exhibit key organizational features at different scales. The nanoscale emphasizes
            a molecular viewpoint, focussing on molecules and their complexes. At the mesoscale, the
            cell is compartmentalised into distinct compartments, each consisting from anywhere from
            hundreds or millions of molecules. Much of the research over the last half century
            focussed on organisation at the nano scale, aided by the concomitant breakthroughs in
            structural analysis of molecules, Research at the nano scale focussed attention on the
            description of protein interactions based on covalent bonds and stereospecific
            interactions. The recent discovery that many mesoscale compartments inside cells are
            liquid-like condensates has focussed attention on how mesoscale organization emerges
            from nano-scale molecular interactions.
          </p>
          <p>
            The problem of how large scale organization emerges from molecular components is a
            classical problem in condensed matter physics, which aims to understand the behaviors
            and physical properties of assemblies of molecules. Condensed matter physics uses
            thermodynamics as a tool, which has the great advantage that it captures system
            properties using a small number of macroscopic variables such as concentration and
            pressure. Condensed matter physics also uses statistical physics to relate these
            thermodynamic variables to the interactions of molecular components. As an example,
            statistical physics can capture how molecular interaction together with the density of
            packing can specify the macroscopic pressure in a material. This suggests that concepts
            in thermodynamics and statistical physics should be useful in describing how the
            mesoscale organization of cells emerges from the properties of individual molecules.
          </p>
          <p>
            The application of thermodynamics and statistical physics to chemistry is called
            physical chemistry. Physical chemistry studies the physics of solutions, chemical
            reactions as well as of materials. Modern physical chemistry was ushered in by
            (Einstein) Ludwig Boltzmann, Josiah W. Gibbs, Kamerlingh Onnes, Ostwald, Arrhenius, Hoff
            and others who developed chemical thermodynamics. Early on, cell biologists realised
            that such an approach could be used to characterise the organisation of cells. For
            instance, in his address in 1913 to the Royal association the British biochemist
            Hopkins, who won the Nobel prize for discovering vitamins, proposed that “the cell is a
            particular dynamic state of a polyphasic system”. Alexander Oparin was a Russian
            physical chemist who together with Haldane proposed phase separation of macro-molecules
            was an early step in the origin of life.
          </p>
        </div>
      </section>

      <section className="section">
        <h2 className="section__title">Thermodynamics and formation of cellular compartments</h2>
        <p className="section__intro">Click on a glossary term to view definition.</p>
        <dl>
          <div className="term term_active">
            <dt className="term__title">Phase separation</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Solubility</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Aggregation</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Condensation</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Condensates</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Density transition</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">emixing</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Droplets</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Coexstence of more than two phases</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Thermodynamics</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Thermodynamics vs kinetics</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Diffusion</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Specificity</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Nucleation</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Nucleator</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Scaffold verus a nucleator</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Spinodal versus nucleation-driven phase separation</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Comparison to Hill coefficients</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Clients and scaffolds</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Partition coefficients</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
          <div className="term">
            <dt className="term__title">Binding constant versus partition coefficients</dt>
            <dd className="term__description">Lorem ipsum dolor</dd>
          </div>
        </dl>
      </section>
    </main>

    {/* aside */}
    <aside className="layout__aside">
      <JumpTo />
      <PopularTerms />
      <BrowseTerms />
    </aside>
  </Layout>
);

export default Page;
