import React from 'react';
import { Link } from 'gatsby';
import './index.scss';

import imgDewpointLogoBlack from './images/dewpoint-logo-black.svg';
import imgDewpointLogoWhite from './images/dewpoint-logo-white.svg';
import imgMenuIcon from './images/menu-icon.svg';
import imgSearchIconBlack from './images/search-icon-black.svg';
import imgSearchIconWhite from './images/search-icon-white.svg';

const Header = props => {
  const { category, title, intro } = props;
  const headerClassName = category && `header_category header_${props.category}`;
  const dewpointLogo = category ? imgDewpointLogoWhite : imgDewpointLogoBlack;
  const searchIcon = category ? imgSearchIconWhite : imgSearchIconBlack;
  return (
    <header id="header" className={headerClassName}>
      {/* add 'nav_closed' class to hide menu on mobile */}
      <nav className="nav grid">
        <Link to="/" className="logo">
          Condensates
        </Link>
        <a className="curated-by" href="./">
          <span className="curated-by__text">Curated by </span>
          <img src={dewpointLogo} alt="Dewpoint" />
        </a>
        <button className="menu-button" aria-hidden="true">
          <img src={imgMenuIcon} alt="" />
        </button>
        <ul className="links">
          <li className="links__menu-title">
            <a href="./">Topics</a>
            <ul className="links__menu">
              <li>
                <a href="./">Neurodegneration</a>
              </li>
              <li>
                <a href="./">Cancer</a>
              </li>
              <li>
                <a href="./">Metabolism</a>
              </li>
              <li>
                <a href="./">Infectious disease</a>
              </li>
              <li>
                <a href="./">Transcription</a>
              </li>
              <li>
                <a href="./">Assay technology</a>
              </li>
              <li>
                <a href="./">Physics of condensates</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="./">Events</a>
          </li>
          <li>
            <a href="./">Jobs</a>
          </li>
          <li>
            <a href="./">Videos</a>
          </li>
          <li>
            <a href="./">Glossary</a>
          </li>
          <li className="links__search">
            <a href="./">
              <img src={searchIcon} alt="Search" />
            </a>
          </li>
        </ul>
      </nav>
      {category && (
        <div className="category-details grid">
          <h1 className="category-details__title">{title}</h1>
          <p className="category-details__intro">{intro}</p>
        </div>
      )}
    </header>
  );
};

export default Header;
