import React from 'react';
import './index.scss';

const Metadata = props => (
  <div id="metadata">
    <table className="table">
      <tbody>
        <tr>
          <th className="table__header">Authors</th>
          <td className="table__data">
            Denes Hnisz, Krishna Shrinivas, Richard A. Young, Arup K. Chakraborty, Phillip A. Sharp
          </td>
        </tr>
        <tr>
          <th className="table__header">Details</th>
          <td className="table__data">Published Mar 23, 2017 | Volume 169 Issue 1</td>
        </tr>
        <tr>
          <th className="table__header">DOI</th>
          <td className="table__data">https://doi.org/10.1016</td>
        </tr>
        <tr>
          <th className="table__header">Topics</th>
          <td className="table__data">
            <ul className="categories">
              <li>
                <a className="category category_cancer" href="./">
                  Cancer
                </a>
              </li>
              <li>
                <a className="category category_infectious-disease" href="./">
                  Infectious Disease
                </a>
              </li>
              <li>
                <a className="category category_transcription" href="./">
                  Transcription
                </a>
              </li>
              <li>
                <a className="category category_condensates-mutation" href="./">
                  Condensates & Mutation
                </a>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <th className="table__header">Keywords</th>
          <td className="table__data">
            <ul className="keywords">
              <li><a href="./">Lorem ipsum</a></li>
              <li><a href="./">Dolor set</a></li>
              <li><a href="./">Tempor</a></li>
              <li><a href="./">Incididunt</a></li>
              <li><a href="./">Exercitation</a></li>
              <li><a href="./">Ullamco</a></li>
              <li><a href="./">Commodo Consequat</a></li>
            </ul>
          </td>
        </tr>
        <tr>
          <th className="table__header">Source</th>
          <td className="table__data">
            <a className="source-link" href="./">View paper on pubmed</a>
          </td>
        </tr>
      </tbody>
    </table>
    <div className="actions">
      <a className="actions__action" href="./">Email this posting</a>
      <button className="actions__action">Copy link</button>
    </div>
  </div>
);

export default Metadata;
