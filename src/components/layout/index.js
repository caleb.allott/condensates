import React from 'react';
import Header from '../header';
import Footer from '../footer';
import './index.scss';

const Layout = props => (
  <div id="layout">
    <Header
      category={props.category}
      title={props.title}
      intro={props.intro}
    />
    <div className="grid layout">
      {props.children}
    </div>
    <Footer />
  </div>
);

export default Layout;
