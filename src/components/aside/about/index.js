import React from 'react';
import './index.scss';

const About = props => (
  <section id="about" className="about">
    <h2 className="about__title">
      <a href="./">About this site</a>
    </h2>
    <p className="about__description">
      Biotherapy news and community. Solving the worlds toughest diseases.
    </p>
    <a className="button" href="./">Contribute</a>
  </section>
);

export default About;
