import React from 'react';

const Terms = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">
      <a href="./">Glossary terms</a>
    </h2>
    <section className="aside-subsection">
      <h3 className="aside-subsection__title">Condensation</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        incididunt ut labore lorem ipsum dolor sit
      </p>
    </section>
    <section className="aside-subsection">
      <h3 className="aside-subsection__title">Phase-separation</h3>
      <p>
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat.
      </p>
    </section>
  </section>
);

export default Terms;
