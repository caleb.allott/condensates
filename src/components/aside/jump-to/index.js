import React from 'react';

const JumpTo = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">Jump to</h2>
    <ul className="aside-list">
      <li className="aside-list__item">
        <a className="aside-list__link aside-list__link_active" href="./">
          About
        </a>
      </li>
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          Introduction
        </a>
      </li>
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          Thermodynamics and formation of ceullar compartments
        </a>
      </li>
    </ul>
  </section>
);

export default JumpTo;
