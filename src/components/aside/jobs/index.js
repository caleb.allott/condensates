import React from 'react';

const Jobs = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">
      <a href="./">Recent jobs</a>
    </h2>
    <section className="aside-subsection">
      <a href="./">
        <h3 className="aside-subsection__title">Technical Assistant</h3>
        <p>
          Company Name
          <br />
          Boston, MA
        </p>
      </a>
    </section>
    <section className="aside-subsection">
      <a href="./">
        <h3 className="aside-subsection__title">Biomolecular Engineer</h3>
        <p>
          Company Name
          <br />
          Munich, Germany
        </p>
      </a>
    </section>
    <section className="aside-subsection">
      <a href="./">
        <h3 className="aside-subsection__title subsection__title_sponsored">
          Automation Scientist, Drug Discovery
        </h3>
        <p>
          Dewpoint Therapeutics
          <br />
          Munich, Germany
        </p>
        <strong className="badge badge_gold">Sponsored</strong>
      </a>
    </section>
  </section>
);

export default Jobs;
