import React from 'react';

const Events = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">
      <a href="./">Upcoming events</a>
    </h2>
    <section className="aside-subsection">
      <a href="./">
        <h3 className="aside-subsection__title">Phase separation galla</h3>
        <p>
          June 29, 2019
          <br />
          Boston, MA
        </p>
      </a>
    </section>
    <section className="aside-subsection">
      <a href="./">
        <h3 className="aside-subsection__title">Phase separation galla</h3>
        <p>
          June 29, 2019
          <br />
          Boston, MA
        </p>
      </a>
    </section>
  </section>
);

export default Events;
