import React from 'react';

const BrowseTerms = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">Browse terms</h2>
    <ul className="aside-list">
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          A <span className="aside-list__detail">4 terms</span>
        </a>
      </li>
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          B <span className="aside-list__detail">2 terms</span>
        </a>
      </li>
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          C <span className="aside-list__detail">7 terms</span>
        </a>
      </li>
    </ul>
  </section>
);

export default BrowseTerms;
