import React from 'react';

import imgVideo1 from './images/video-1.png';
import imgVideo12x from './images/video-1@2x.png';
import imgVideo2 from './images/video-2.png';
import imgVideo22x from './images/video-2@2x.png';

const Videos = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">
      <a href="./">Videos</a>
    </h2>
    <figure className="aside-subsection video">
      <a href="./">
        <img
          className="video__thumbnail"
          src={imgVideo1}
          srcSet={`${imgVideo1} 1x, ${imgVideo12x} 2x`}
          alt=""
        />
        <figcaption className="video__caption">
          Tony Hyman on how biomolecular condensates can treat disease.
        </figcaption>
      </a>
    </figure>
    <figure className="aside-subsection video">
      <a href="./">
        <img
          className="video__thumbnail"
          src={imgVideo2}
          srcSet={`${imgVideo2} 2x, ${imgVideo22x} 2x`}
          alt=""
        />
        <figcaption className="video__caption">
          Function and Regulation of Phase-Separated Biological Condensates
        </figcaption>
      </a>
    </figure>
  </section>
);

export default Videos;
