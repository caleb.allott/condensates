import React from 'react';

const PopularTerms = props => (
  <section className="aside-section">
    <h2 className="aside-section__title">Popular terms</h2>
    <ul className="aside-list">
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          Phase Separation
        </a>
      </li>
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          Biomolecular Condensates
        </a>
      </li>
      <li className="aside-list__item">
        <a className="aside-list__link" href="./">
          Liquid-Liquid
        </a>
      </li>
    </ul>
  </section>
);

export default PopularTerms;
