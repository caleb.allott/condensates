import React from 'react';
import { Link } from 'gatsby';
import './index.scss';

const Footer = () => (
  <footer id="footer">
    <nav className="nav grid">
      <section className="section section_condensates">
        <h6 className="section__title section__title_blue"><a href="./">Condensates.org</a></h6>
        <ul>
          <li><a href="./">Share an article</a></li>
          <li><a href="./">Learn more</a></li>
          <li><a href="./">Follow us</a></li>
        </ul>
      </section>
      <section className="section section_dewpoint">
        <h6 className="section__title section__title_gray"><Link to="/">Dewpoint Therapeutics</Link></h6>
        <ul>
          <li><Link to="/team">Team</Link></li>
          <li><Link to="/our-approach">Our approach</Link></li>
          <li><Link to="/careers">Careers (22 positions)</Link></li>
          <li><Link to="/news">News</Link></li>
          <li><Link to="/contact">Press & Contact</Link></li>
        </ul>
      </section>
      <section className="section section_contact-us">
        <h6 className="section__title section__title_gold"><Link to="/contact">Contribute</Link></h6>
        <p>
          Share a news posting, event, job or video on Condensates.com
        </p>
        <Link to="/contact" className="section__button button">Contribute</Link>
      </section>
    </nav>
    <div className="legal">
      <div className="grid">
        <small className="legal__inner">
          <p>Dewpoint Therapeutics</p>
          <ul>
            <li className="legal__link"><a href="./">Privacy</a></li>
            <li className="legal__link"><a href="./">Terms</a></li>
          </ul>
        </small>
      </div>
    </div>
  </footer>
);

export default Footer;
